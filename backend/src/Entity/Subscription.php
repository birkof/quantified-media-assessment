<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use App\Repository\SubscriptionRepository;
use App\State\UserProfileProvider;
use App\State\UserSubscribeToSubscriptionProcessor;
use App\State\UserSubscriptionProvider;
use App\State\UserUnsubscribeToSubscriptionProcessor;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableTrait;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: SubscriptionRepository::class)]
#[ORM\Table(name: 'subscriptions')]
#[Get(
    uriTemplate: '/subscriptions/me',
    normalizationContext: ['groups' => ['user_subscription']],
    provider: UserSubscriptionProvider::class,
)]
#[Post(
    uriTemplate: '/subscriptions/{id}/subscribe.{_format}',
    requirements: ['id' => '\d+'],
    normalizationContext: ['groups' => ['subscribe_to_subscription']],
    input: false,
    name: 'subscription_subscribe',
    processor: UserSubscribeToSubscriptionProcessor::class,
)]
#[Post(
    uriTemplate: '/subscriptions/{id}/unsubscribe.{_format}',
    requirements: ['id' => '\d+'],
    input: false,
    output: false,
    name: 'subscription_unsubscribe',
    processor: UserUnsubscribeToSubscriptionProcessor::class,
)]
#[ApiResource(
    operations: [
        new Get(),
        new GetCollection()
    ]
)]
class Subscription implements TimestampableInterface
{
    use TimestampableTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['user_subscription', 'subscribe_to_subscription'])]
    private ?string $name = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $description = null;

    #[ORM\Column]
    #[Groups(['subscribe_to_subscription'])]
    private ?float $price = null;

    #[ORM\Column]
    #[Groups(['subscribe_to_subscription'])]
    private ?int $duration = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): static
    {
        $this->price = $price;

        return $this;
    }

    public function getDuration(): ?int
    {
        return $this->duration;
    }

    public function setDuration(int $duration): static
    {
        $this->duration = $duration;

        return $this;
    }
}
