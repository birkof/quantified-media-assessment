<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\User;
use App\Repository\UserRepository;
use Exception;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use UnexpectedValueException;

#[AsCommand(
    name: 'erp:core:user:admin',
    description: 'Create users',
    aliases: ['user-admin', 'admin']
)]
class UserAdminCommand extends Command
{
    private InputInterface $input;
    private OutputInterface $output;
    private SymfonyStyle $io;

    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly UserPasswordHasherInterface $hasher,
    ) {
        parent::__construct();
    }

    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ): int {
        $this->input = $input;
        $this->output = $output;
        $this->io = new SymfonyStyle($input, $output);

        $this->io->title('User Admin CLI Interface');

        do {
            $exit = $this->showMenu();
        } while (!$exit);

        return Command::SUCCESS;
    }

    private function showMenu(): int
    {
        $answer = $this->getAnswer();
        $this->output->writeln($answer);

        try {
            switch ($answer) {
                case 'Create User':
                    $this->createUser();
                    $this->io->success('User created');
                    break;
                case 'Read Users':
                    $this->renderUsersTable();
                    break;
                case 'Update User':
                    $this->io->warning('Update is not implemented yet :(');
                    break;
                case 'Delete User':
                    $this->deleteUser();
                    $this->io->success('User has been removed');
                    break;
                case 'Exit':
                    $this->io->text('have Fun =;)');

                    return Command::FAILURE;
                default:
                    throw new UnexpectedValueException('Unknown answer: ' . $answer);
            }
        } catch (Exception $exception) {
            $this->io->error($exception->getMessage());
        }

        return Command::SUCCESS;
    }

    private function getAnswer(): string
    {
        $question = (new ChoiceQuestion(
            'Please select an option (defaults to exit)',
            [
                'Exit',
                'Create User',
                'Read Users',
                'Update User',
                'Delete User',
            ],
            0
        ))
            ->setErrorMessage('Choice %s is invalid.');

        return $this->getHelper('question')->ask(
            $this->input,
            $this->output,
            $question
        );
    }

    private function renderUsersTable(): void
    {
        $table = new Table($this->output);
        $table->setHeaders(['ID', 'Email', 'Roles']);

        $users = $this->userRepository->findBy([], ['id' => 'ASC']);

        $this->io->text(
            sprintf(
                '<fg=cyan>There are %d users in the database.</>',
                count($users)
            )
        );

        foreach ($users as $user) {
            $table->addRow(
                [
                    $user->getId(),
                    $user->getUserIdentifier(),
                    implode(', ', $user->getRoles()),
                ]
            );
        }
        $table->render();
    }

    private function askEmail(): string
    {
        $io = new SymfonyStyle($this->input, $this->output);
        do {
            $identifier = $this->getHelper('question')->ask(
                $this->input,
                $this->output,
                new Question('Email address: ')
            );
            if (!$identifier) {
                $io->warning('Email address required :(');
            }
        } while ($identifier === null);

        return $identifier;
    }

    private function askFullName(): string
    {
        $io = new SymfonyStyle($this->input, $this->output);
        do {
            $fullName = $this->getHelper('question')->ask(
                $this->input,
                $this->output,
                new Question('Full Name: ')
            );
            if (!$fullName) {
                $io->warning('Full Name required :(');
            }
        } while ($fullName === null);

        return $fullName;
    }

    private function askPassword(): string
    {
        $io = new SymfonyStyle($this->input, $this->output);
        do {
            $password = $this->getHelper('question')->ask(
                $this->input,
                $this->output,
                new Question('Password: ')
            );
            if (!$password) {
                $io->warning('Password required :(');
            }
        } while ($password === null);

        return $password;
    }

    private function askRole(): mixed
    {
        return $this->getHelper('question')->ask(
            $this->input,
            $this->output,
            (new ChoiceQuestion(
                'User role',
                array_values(User::extractAvailableRoles()->toArray())
            ))
                ->setErrorMessage('Choice %s is invalid.')
        );
    }

    private function createUser(): void
    {
        $email = $this->askEmail();
        $name = $this->askFullName();
        $password = $this->askPassword();
        $role = $this->askRole();

        $user = new User();
        $user
            ->setEmail($email)
            ->setFullName($name)
            ->setPassword($this->hasher->hashPassword($user, $password))
            ->addRole($role)
            ->setCreatedBy(__CLASS__)
        ;

        $this->userRepository->save($user, true);
    }

    private function deleteUser(): void
    {
        $id = $this->getHelper('question')->ask(
            $this->input,
            $this->output,
            new Question('User ID to delete: ')
        );
        $user = $this->userRepository->findOneById($id);

        if (!$user) {
            throw new UnexpectedValueException('User not found!');
        }

        $this->userRepository->remove($user, true);
    }
}
