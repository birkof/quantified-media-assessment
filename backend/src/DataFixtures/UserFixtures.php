<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends BaseFixture
{
    private const DEFAULT_USER_PASSWORD = 'testinel';

    public function __construct(private readonly UserPasswordHasherInterface $passwordEncoder)
    {
    }

    public function loadData(ObjectManager $manager): void
    {
        $this->createMany(User::class, 5, function (User $user, $count) {
            $hashedPassword = $this->passwordEncoder->hashPassword($user, self::DEFAULT_USER_PASSWORD);
            $user
                ->setEmail($this->faker->safeEmail)
                ->setFullName(sprintf('%s %s', $this->faker->firstName, $this->faker->lastName))
                ->addRole(User::ROLE_USER)
                ->setPassword($hashedPassword)
            ;
        });

        $manager->flush();
    }
}
