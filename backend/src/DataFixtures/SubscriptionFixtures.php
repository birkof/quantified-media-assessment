<?php

namespace App\DataFixtures;

use App\Entity\Subscription;
use Doctrine\Persistence\ObjectManager;

class SubscriptionFixtures extends BaseFixture
{
    private const DEFAULT_SUBSCRIPTIONS = [
         ['name' => 'trial', 'price' => 0, 'duration_in_day' => 5],
         ['name' => 'month', 'price' => 100, 'duration_in_day' => 30],
         ['name' => 'bi-annual', 'price' => 450, 'duration_in_day' => 180],
         ['name' => 'year', 'price' => 1000, 'duration_in_day' => 365],
    ];

    public function loadData(ObjectManager $manager): void
    {
        $this->createMany(Subscription::class, 4, function (Subscription $subscription, $count) {
            $subscription
                ->setName(self::DEFAULT_SUBSCRIPTIONS[$count]['name'])
                ->setDescription($this->faker->randomLetter)
                ->setPrice(self::DEFAULT_SUBSCRIPTIONS[$count]['price'])
                ->setDuration(self::DEFAULT_SUBSCRIPTIONS[$count]['duration_in_day'])
            ;
        });

        $manager->flush();
    }
}
