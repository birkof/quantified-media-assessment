import type {IUser} from '@/api/types'
import {defineStore} from 'pinia'

export type AuthStoreState = {
    user: IUser | null
}

export const useAuthStore = defineStore('authStore', {

    state: () => {
        return {
            user: null as IUser | null,
            accessToken: '',
            refreshToken: ''
        }
    },
    getters: {},
    actions: {}
})
