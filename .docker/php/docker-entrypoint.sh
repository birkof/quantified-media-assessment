#!/bin/sh
set -e

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
	set -- php-fpm "$@"
fi

if [ "$1" = 'php-fpm' ] || [ "$1" = 'php' ] || [ "$1" = 'bin/console' ]; then
  if [ ! -d /srv/app/var ]; then
	mkdir -p /srv/app/var/cache /srv/app/var/log
  fi

  if [ ! -d /srv/app/vendor ]; then
      echo "Installing vendor files..."
      php /usr/bin/composer install --prefer-dist --no-dev --no-autoloader --no-scripts --no-progress; \
      php /usr/bin/composer clear-cache; \
  fi

  setfacl -R -m u:www-data:rwX -m u:"$(whoami)":rwX var
  setfacl -dR -m u:www-data:rwX -m u:"$(whoami)":rwX var
fi

exec docker-php-entrypoint "$@"
