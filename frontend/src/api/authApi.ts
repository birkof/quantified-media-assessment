import axios from 'axios'
import { useAuthStore } from '@/stores/authStore'
import type {
  GenericResponse,
  ILoginInput,
  ILoginResponse,
  IUserResponse
} from './types'

const BASE_URL = `${import.meta.env.VITE_BACKEND_API_URL}/api`
const authApi = axios.create({
  baseURL: BASE_URL,
  withCredentials: true
})

authApi.defaults.headers.common['Content-Type'] = 'application/json'
authApi.defaults.headers.common['Accept'] = 'application/json'

export const refreshAccessTokenFn = async () => {
  const response = await authApi.post<ILoginResponse>('/token/refresh')
  return response.data
}

authApi.interceptors.request.use(
  (request) => {
    const authStore = useAuthStore()

    const accessToken = authStore.accessToken
    if (accessToken) {
      request.headers.Authorization = `Bearer ${accessToken}`
    }

    return request
  },
  async (error) => {
    return Promise.reject(error)
  }
)

authApi.interceptors.response.use(
  (response) => {
    return response
  },
  async (error) => {
    const originalRequest = error.config
    const errMessage = error.response.data.message as string
    if (errMessage.includes('not logged in') && !originalRequest._retry) {
      originalRequest._retry = true
      await refreshAccessTokenFn()
      return authApi(originalRequest)
    }
    return Promise.reject(error)
  }
)

export const loginUserFn = async (user: ILoginInput) => {
  const response = await authApi.post<ILoginResponse>('/login', user)
  return response.data
}

export const logoutUserFn = async () => {
  const response = await authApi.get<GenericResponse>('/logout')
  return response.data
}

export const getMeFn = async () => {
  const response = await authApi.get<IUserResponse>('/users/me')
  return response.data
}
