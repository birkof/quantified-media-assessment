<?php

namespace App\Service;

use App\Entity\Subscription;
use App\Entity\User;
use App\Entity\UserSubscription;
use App\Repository\UserRepository;
use App\Repository\UserSubscriptionRepository;
use Carbon\Carbon;
use Doctrine\Common\Collections\Collection;

class SubscriptionService
{
    public function __construct(
        private readonly UserSubscriptionRepository $userSubscriptionRepository,
        private readonly UserRepository $userRepository,
    ) {
    }

    public function subscribeUserToSubscription(User $user, Subscription $subscription): UserSubscription
    {
        $userSubscription = $this->userSubscriptionRepository->findOneBy(['user' => $user, 'subscription' => $subscription]);

        // Return it in case it exists
        if ($userSubscription) {
            return $userSubscription;
        }

        // Otherwise, create it
        $userSubscription = (new UserSubscription())
            ->setSubscription($subscription)
            ->setStatus(UserSubscription::STATUS_ENABLED)
            ->setStartDate(Carbon::now())
            ->setEndDate(Carbon::now()->addDays($subscription->getDuration()));

        $user->addSubscription(
            $userSubscription
        );

        $this->userRepository->save($user, true);

        return $userSubscription;
    }

    public function unsubscribeUserFromSubscription(User $user, Subscription $subscription): Collection
    {
        $userSubscription = $this->userSubscriptionRepository->findOneBy(['user' => $user, 'subscription' => $subscription]);

        // Remove it in case it exists
        if ($userSubscription) {
            $this->userSubscriptionRepository->remove($userSubscription, true);
        }

        return $user->getSubscriptions();
    }
}
