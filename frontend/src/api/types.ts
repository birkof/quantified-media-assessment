export interface IUser {
  fullName: string
  email: string
  roles: any
  id: number
  created_at: string
  updated_at: string
}

export interface GenericResponse {
  status: string
  message: string
}

export interface ILoginInput {
  email: string
  password: string
}

export interface ILoginResponse {
  token: string
  refresh_token: string
  refresh_token_expiration: number
}

export interface IUserResponse {
  status: string
  data: {
    user: IUser
  }
}
