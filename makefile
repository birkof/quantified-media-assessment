DEFAULT_GOAL := help

DOCKER_COMPOSE_FILE=./.docker/docker-compose.yaml
PROJECT_NAME=quantified-media

# COLORS
RESET = \033[0m
BLUE = \033[36m
GREEN = \033[0;32m
RED = \033[0;31m

.PHONY: all clean fclean re

help:
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make ${BLUE}[target]${RESET}\n"}/^[a-zA-Z0-9_-]+:.*?##/ \
	{ printf "  ${RED}%-10s${RESET}%s\n", $$1, $$2 } /^##@/ \
	{ printf "\n${BLUE}%s${RESET}\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

all: ## build & run & install
	@$(MAKE) build
	@$(MAKE) build-ui
	@$(MAKE) start
	@$(MAKE) install
	@$(MAKE) migrations
	@$(MAKE) fixtures
	@$(MAKE) jwt

build: ## build images and containers
	@docker compose -f $(DOCKER_COMPOSE_FILE) -p ${PROJECT_NAME} build --compress --force-rm --no-cache --pull
	@$(MAKE) clean

install: ## install dependencies
	@docker compose -f $(DOCKER_COMPOSE_FILE) -p ${PROJECT_NAME} exec php composer install

build-ui: ## build ui
	@docker run --rm -it -v ${PWD}/frontend:/srv/app -w /srv/app node:alpine npm install
	@docker run --rm -it -v ${PWD}/frontend:/srv/app -w /srv/app node:alpine npm run build

migrations: ## run migrations
	@docker compose -f $(DOCKER_COMPOSE_FILE) -p ${PROJECT_NAME} exec php console doctrine:schema:drop --full-database --no-interaction --force
	@docker compose -f $(DOCKER_COMPOSE_FILE) -p ${PROJECT_NAME} exec php console doctrine:migrations:migrate --no-interaction --allow-no-migration

fixtures: ## run data fixtures
	@docker compose -f $(DOCKER_COMPOSE_FILE) -p ${PROJECT_NAME} exec php console doctrine:fixtures:load --no-interaction

jwt: ## generate the SSL keys for JWT token
	@docker compose -f $(DOCKER_COMPOSE_FILE) -p ${PROJECT_NAME} exec php console lexik:jwt:generate-keypair --overwrite --no-interaction

start: ## start the containers
	@docker compose -f $(DOCKER_COMPOSE_FILE) -p ${PROJECT_NAME} up -d

stop: ## stop the containers
	@docker compose -f $(DOCKER_COMPOSE_FILE) -p ${PROJECT_NAME} down

remove: ## stop the containers & remove volumes
	@docker compose -f $(DOCKER_COMPOSE_FILE) -p ${PROJECT_NAME} down -v

restart: ## restart the containers
	@$(MAKE) stop
	@$(MAKE) start

shell: ## access to the container
	@docker compose -f $(DOCKER_COMPOSE_FILE) -p ${PROJECT_NAME} exec php /bin/sh

logs: ## display the logs in bash
	@docker compose -f $(DOCKER_COMPOSE_FILE) -p ${PROJECT_NAME} logs -f

clean: ## clean docker
	@docker system prune --volumes --force

psalm: ## Run psalm static analysis
	@docker compose -f $(DOCKER_COMPOSE_FILE) -p ${PROJECT_NAME} exec php ./vendor/bin/psalm --show-info=true

phpunit: ## Run unit tests by PHP Unit
	@docker compose -f $(DOCKER_COMPOSE_FILE) -p ${PROJECT_NAME} exec php ./bin/phpunit

style: ## Run tests by PHP Mess Detector
	@docker compose -f $(DOCKER_COMPOSE_FILE) -p ${PROJECT_NAME} exec php ./vendor/bin/phpmd src text phpmd.xml --exclude "migrations/**,src/Kernel.php"

cs: ## Run tests by PHP_CodeSniffer
	@docker compose -f $(DOCKER_COMPOSE_FILE) -p ${PROJECT_NAME} exec php ./vendor/bin/phpcs --standard=./phpcs.xml --ignore=src/Kernel.php

cs-fix: ## Run PHP Code Beautifier and Fixer
	@docker compose -f $(DOCKER_COMPOSE_FILE) -p ${PROJECT_NAME} exec php ./vendor/bin/phpcbf --standard=./phpcs.xml --ignore=src/Kernel.php

fclean: clean
	@rm -rf ./var
	@rm -rf ./vendor/

re:
	@$(MAKE) stop
	@$(MAKE) fclean
	@$(MAKE) all
