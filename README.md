# Subscription Management System (Test assignment)

A boilerplate for a Subscription Management System using Symfony as framework and running with latest PHP 8.2


## Project Setup
To start the entire stack you need to use the following command
```bash
make all
```

List installed users by typing the following
```bash
make shell
console admin // pick 2 to extract users from database
```

>PS: Follow the URLs in the Implementations section


## Implementations

- [x] Environment in Docker
- [x] Rest API - http://localhost/api
- [x] Swagger API Doc http://localhost/api/docs
- [x] VueJs Frontend http://localhost:8080

## Stack

- PHP 8+
- Mysql 8.0
- Nginx
- Nodejs

## Project Actions

| Action        	   | Command         |
|-------------------|-----------------|
| Setup 	           | `make all`      |
| Start 	           | `make start`    |
| Run Tests       	 | `make phpunit`  |
| Static Analisys 	 | `make style`  	 |
| Code Style      	 | `make cs`     	 |
| Code style check	 | `make cs-check` |
| PHP Shell 	       | `make shell`    |


## Postman

Attached you'll find a Postman collection with all the requests exposed.


## Author
[Daniel Stancu](https://github.com/birkof) 
