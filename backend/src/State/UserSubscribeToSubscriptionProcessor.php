<?php

namespace App\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\Entity\Subscription;
use App\Entity\User;
use App\Service\SubscriptionService;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserSubscribeToSubscriptionProcessor implements ProcessorInterface
{
    public function __construct(private readonly Security $security, private readonly SubscriptionService $subscriptionService)
    {
    }

    public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = [])
    {
        /** @var User $user */
        $user = $this->security->getUser();

        /** @var Subscription $subscription */
        $subscription = $data;

        if (!$subscription) {
            throw new NotFoundHttpException('Subscription not found.');
        }

        return $this->subscriptionService->subscribeUserToSubscription($user, $subscription);
    }
}
